#!/usr/bin/env node

import * as https from 'https';
import * as dotenv from 'dotenv';
import ScreepsUtil, { ScreepsModuleObject } from '../utils/ScreepsUtil';

interface EnvDeployment {
    EMAIL?: string,
    PASSWORD?: string,
    BRANCH?: string,
}

const config: EnvDeployment = dotenv.config().parsed as EnvDeployment;

ScreepsUtil.getModules('dist').then((modules: ScreepsModuleObject) => {
    const data = {
        branch: config.BRANCH,
        modules: modules,
    };

    const req = https.request({
        hostname: 'screeps.com',
        port: 443,
        path: '/api/user/code',
        method: 'POST',
        auth: `${config.EMAIL}:${config.PASSWORD}`,
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
        },
    });

    req.end(JSON.stringify(data), (a: any) => console.log(a));
});
