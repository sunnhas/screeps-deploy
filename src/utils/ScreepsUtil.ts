import * as glob from 'glob';
import { trimStart } from 'lodash';
import { PathLike, readFile } from 'fs';

export default class ScreepsUtil {
    public static getModules(baseDir: PathLike): Promise<ScreepsModuleObject> {
        if (typeof baseDir === 'string') {
            baseDir = trimStart(baseDir, '/');
        }

        return new Promise<ScreepsModuleObject>((resolve, reject) => {
            glob(`${baseDir}/**/*.js`, (error: Error, files: string[]) => {
                if (error) {
                    return reject(error);
                }

                const fileObject: ScreepsModuleObject = {};
                const promises: Promise<void>[] = [];

                files.forEach((file: string) => {
                    const promise = File.read(file).then((content) => {
                        const fileName = file.replace(`${baseDir}/`, '')
                            .replace('/', '.').replace('.js', '');
                        
                        fileObject[fileName] = content;
                    });

                    promises.push(promise);
                });

                Promise.all(promises).then(() => resolve(fileObject));
            });
        });
    }
}

export interface ScreepsModuleObject {
    [key: string]: string | Buffer
}




interface ReadFileOptions {
    encoding?: string | null;
    flag?: string;
}

class File {
    public static read(path: PathLike, options?: ReadFileOptions): Promise<string | Buffer> {
        if (! options) {
            options = {};
        }
        
        return new Promise<string>((resolve, reject) => {
            readFile(path, options, (error: NodeJS.ErrnoException, data: string | Buffer) => {
                if (error) {
                    return reject(error);
                }
                
                if (data instanceof Buffer) {
                    data = data.toString();
                }
                
                resolve(data);
            });
        });
    }
}
